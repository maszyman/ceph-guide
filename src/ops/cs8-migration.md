# Centos Stream 8 migration

All the information regarding centos stream 8 can be found in this
[document](https://codimd.web.cern.ch/mclvjGymSFmZGOZeDkEqmA?view).

## Upgrading from Centos 8 in place

1.  Create new CS8 nodes with representative configurations and validate

2.  Enable the upgrade (top-level hostgroup, sub-hostgroup, etc)

        base::migrate::stream8: true

3.  Follow the [instructions](https://linux.web.cern.ch/centos8/docs/migration/)
    -   Run Puppet twice.
    -   Run `distro-sync`.
    -   Reboot.
