# Ceph Upgrade General Procedure

## Wait and Test

Wait at least a week or two after upstream has released a version before considering an upgrade to our prod clusters.
Pay attention to the ceph-users ML in case any regressions appear.

Carry out the upgrade on small, less important clusters first, before upgrading large important clusters.

## Scheduling, SSB

Check which clusters require to create an SSB ([CEPH-558](https://its.cern.ch/jira/browse/CEPH-558)):
  * If needed, always create it one week in advance. Here is an example: https://cern.service-now.com/service-portal/view-outage.do?n=OTG0055716
  * If not, consider if the upgrade should be communicated via email // mattermost to interested people

Plan around half a day for a large cluster upgrade.

## Procedure

The release notes always describe the exact upgrade procedure, e.g. https://docs.ceph.com/docs/master/releases/nautilus/

Note: before upgrading, you should know if `yum upgrade` of the ceph rpms restarts the ceph.target systemd unit.
This is important to know because in general we want to decouple the rpm upgrade from the daemon restarts.

In one terminal, keep this running the entire upgrade: `watch "ceph -s && ceph versions"`.
Between each upgrade step, we need all PGs to be active, no PGs to be degraded, no PGs to be backfilling.

In general:

  * Inform your colleagues that the upgrade starts (MM ~Ceph-Internal)
  * `ceph osd set noout; ceph osd set noin; ceph balancer off`
  * Remove the `ceph-debuginfo` rpm from all hosts -- it will slow down the yum upgrades.
  * upgrade mons (peons first, leader last)
    * `systemctl restart ceph-mon.target`
  * upgrade mgrs (standby first, active last)
    * `systemctl restart ceph-mgr.target`
  * upgrade osds (one host/rack at a time depending on crush rules)
    * `systemctl restart ceph-osd.target`
  * upgrade mdss (if relevant -- note that mds have a [special upgrade procedure](https://docs.ceph.com/docs/nautilus/cephfs/upgrading/), *decrease to ONE active*)
    * `systemctl restart ceph-mds.target`
  * upgrade rgws (if relevant -- special nomad procedure, see S3.md)
  * `ceph osd unset noout; ceph osd unset noin; ceph balancer on`
  * Inform your colleagues that the upgrade has finished (MM ~Ceph-Internal)

MCollective can be useful. Here some examples:

  * Find all machines in one hostgroup: `mco find -T ceph -F hostgroup_1=levinson --dt=3`
  * Check ceph package version: `mco package status ceph -T ceph -F hostgroup_1=levinson --dt=3`
  * Update the ceph packages in the whole hostgroup: `mco shell run 'yum update -y ceph' -T ceph -F hostgroup_1=levinson --dt=3`
  * Restart all OSDs in a given rack: `mco shell run 'systemctl restart ceph-osd.target' -T ceph -F hostgroup_1=levison -F landb_rackname=SE04 --dt=3`


## Known Issues

### ceph-mds out-of-memory during rejoin

The ceph-mds can run out-of-memory during rejoin if the openfilestable is huge (e.g. on the ceph/kelly cluster used by Kopano).
If this is happening, the workaround is to remove the openfilestable object for the relevant rank `X`:

```
rados -p cephfs_metadata rm mdsX_openfiles.0
```
