# S3 operations notes

## _Deprecation Notice_
Since July 2021, S3 uses a different frontend without consul and nomad. If you are looking for the new docs, please refer to [https://gitlab.cern.ch/ceph/ceph-guide/-/blob/master/src/ops/s3.md](https://gitlab.cern.ch/ceph/ceph-guide/-/blob/master/src/ops/s3.md).

Relevant hostgroups, puppet manifests, etc. of old infra based on consul and nomad:
- Consul + Nomad cluster heads:
  - hostgroup: `ceph/consul`
  - puppet: https://gitlab.cern.ch/ai/it-puppet-hostgroup-ceph/-/blob/master/code/manifests/consul.pp
  - alias: `ceph-consul-meyrin.cern.ch`
  - status: 3 VMs active (raft consensus), Consul v1.8.4, Nomad v0.12.5
- Worker nodes (RGWs + Traefik):
  - hostgroup: `ceph/gabe/radosgw/hashi`
  - puppet: https://gitlab.cern.ch/ai/it-puppet-hostgroup-ceph/-/blob/master/code/manifests/gabe/radosgw/hashi.pp
  - alias: `s3.cern.ch`
  - status: All jobs stopped. All VMs killed.
- Nomad jobs for RGWs, Traefik, and Filebeat: https://gitlab.cern.ch/ceph/nomad-rgw
- Logstash config on Monit Marathon for S3 access logs: https://gitlab.cern.ch/ceph/s3logs-to-es/-/tree/s3logs_old (branch: `s3logs_old`)


## About the architecture

The CERN S3 service is provided by a Ceph cluster named `gabe` and an arbitrary number of `radosgw` running on disposable VMs.
Each node in the `ceph/gabe/radosgw` hostgroup also runs a reverse-proxy (træfik) in order to spread the load on the VMs
running a `radosgw`.

## Components

- RadosGW: daemon handling S3 requests and interacting with the Ceph cluster
- Consul: distributed key-value store and service discovery system (used to bootstrap `nomad` and stores `træfik` secrets)
- Nomad: job scheduler that makes sure every node runs a `træfik` process and that the configured number of `radosgw` are running in the cluster.
- Træfik: reverse proxy: handles HTTP(S) requests from the Internet and spreads the load on `radosgw` daemons it discovered by querying the `consul` catalog.

## Useful documentation

- Nomad job specification: (https://www.nomadproject.io/docs/job-specification/index.html)
- Consul reference doc: (https://www.consul.io/docs/index.html)
- Consul guide: (https://learn.hashicorp.com/consul/)
- Træfik documentation: (https://docs.traefik.io/)
- S3 Script guide: (https://gitlab.cern.ch/ceph/ceph-guide/-/blob/master/src/ops/s3-scripts.md)

## Dashboards

- Træfik: http://s3.cern.ch/traefik/
- Nomad: https://ceph-consul-meyrin.cern.ch:4646/
- Consul: http://localhost:8500/ui/   (after `ssh -L8500:localhost:8500 ceph-consul-meyrin`)

## Upgrading S3

### Upgrading mgr/rgw components

To upgrade the mgr and rgw in `ceph-gabe`, proceed as follows on each host: 

 - `systemctl stop ceph.target` (you may want to use `killall` if the mgr refuse to stop)
 - `yum update` to update the packages (check that the ceph package is actually upgraded) 
 - `systemctl start ceph.target`

### Upgrading osds:

To upgrade the osds, set first the following flag on the Ceph cluster:

 - `ceph osd set noin` 
 - `ceph osd set noout`
 
Then for each host (you may perform the osd upgrade rack-wide): 

 - `systemctl stop ceph.target` (you may want to use `killall` if osds are not willing to stop)
 - `yum update`
 - `systemctl start ceph.target`

Don't forget to unset the flags: 

 - `ceph osd unset noin`
 - `ceph osd unset nout`

### Upgrading consul

To upgrade Consul, proceed as follows:
 - Check the [documentation](https://www.consul.io/docs/upgrading/upgrade-specific) and the [changelog](https://github.com/hashicorp/consul/blob/master/CHANGELOG.md) for further instruction or incompatibilities
 - Disable puppet on all the machines involved, both servers (alias `ceph-consul-meyrin.cern.ch`) and worker nodes (alias `s3.cern.ch`)
 - Set the version to upgrade to in the puppet manifest:
   * [Server](http://gitlab.cern.ch/ai/it-puppet-hostgroup-ceph/-/blob/master/data/hostgroup/ceph/consul.yaml) -- for `ceph-consul-meyrin` nodes
   * [Client](https://gitlab.cern.ch/ai/it-puppet-hostgroup-ceph/-/blob/master/data/hostgroup/ceph.yaml) -- for `s3.cern.ch` nodes
 - Server nodes first: Enable and run puppet manually on each node, one at a time. Make sure the node is back in the pool and reports the right version:
 ```
 watch -n 1 consul members
 ```
 - Client nodes after: Same as with server nodes.

_Note:_ The client nodes might report warning/error messages in `/var/log/messages` during the upgrade. This is normal and the messages will disappear once consul is running again.
```
[WARN ] consul.sync: failed to update services in Consul: error="error querying Consul services: Get http://localhost:8500/v1/agent/services: dial tcp [::1]:8500: connect: connection refused"
[INFO ] client.fingerprint_mgr.consul: consul agent is unavailable
[INFO ] client: node registration complete
[ERROR] consul.sync: still unable to update services in Consul: failures=10 error="error querying Consul services: Get http://localhost:8500/v1/agent/services: dial tcp [::1]:8500: connect: connection refused"
```

### Upgrading nomad

Before proceeding, keep in mind that:
 - Downgrading is not (easily) possible
 - Backward compatibility is maintained for 1 point release (e.g., v0.8 to v0.9)
 - Upgrading 2 point releases (e.g., v0.8 to v0.10) is untested
 - In-place update should be preferred as running allocations will not be interrupted

_Note:_ To allow for in-place updates, one should set a proper `heartbeat_grace` value (default is 10s) on nomad servers.
Consider increasing this value to 120s (or more) at [nomad/server.pp](https://gitlab.cern.ch/ai/it-puppet-hostgroup-ceph/-/blob/master/code/manifests/classes/nomad/server.pp):
```
    config_hash   => {
      'server'     => {
        'enabled'          => true,
        'bootstrap_expect' => 3,
        'heartbeat_grace'  => '120s',
      },
```

To upgrade Nomad, proceed as follows:
 - Check the [documentation](https://www.nomadproject.io/docs/upgrade/upgrade-specific) for further instruction or incompatibilities
 - Disable puppet on all the machines involved, both servers (alias `ceph-consul-meyrin.cern.ch`) and worker nodes (alias `s3.cern.ch`)
 - Set the version to upgrade to in the puppet manifest:
   * [Server](http://gitlab.cern.ch/ai/it-puppet-hostgroup-ceph/-/blob/master/data/hostgroup/ceph/consul.yaml) -- for `ceph-consul-meyrin` nodes
   * [Client](https://gitlab.cern.ch/ai/it-puppet-hostgroup-ceph/-/blob/master/data/hostgroup/ceph.yaml) -- for `s3.cern.ch` nodes
 - Server nodes first: Enable and run puppet manually on each node, one at a time. Make sure the node is back in the pool and reports the right version:
 ```
 watch -n 1 nomad server members
 ```

 Also check that the upgraded server can follow the index of the raft algorithm by comparing the `applied_index` variable on the leader and the server node:
 ```
 nomad agent-info
 ```
 - Client nodes after: Same as with server nodes. Tip: Run it on one client node and make sure allocations are not rescheduled ot run `heartbeat_grace` accordingly.
 - Revert `heartbeat_grace` in the nomad server configuration to the default value (10s).


### Upgrade Docker

_Warning:_ Before proceeding, keep in mind that in-place update of Docker is disruptive as containers (e.g., traefik, filebeat, etc.) will be stopped and restarted.
This might lead to service downtime. Make sure two or more allocations are available on different nodes and that these nodes are in the load-balanced alias.

To upgrade Docker, proceed as follows (example for `ceph-consulworker-testing-794c2396aa.cern.ch`):
 - Remove  the machine from the load balancer (and wait for it to happen)
 ```
 roger update --appstate=intervention
 ```
 - Drain the node with nomad
 ```
 [root@ceph-consul-testing-91901a3674 ~]# nomad node status
 ID        DC      Name                                          Class    Drain  Eligibility  Status
 2fc1eb64  testdc  ceph-consulworker-testing-ced6aba248.cern.ch  compute  false  eligible     ready
 e144f159  testdc  ceph-consulworker-testing-794c2396aa.cern.ch  compute  false  eligible     ready
 4cad6b1f  testdc  ceph-consulworker-testing-0cbc062053.cern.ch  compute  false  eligible     ready
 
 [root@ceph-consul-testing-91901a3674 ~]# nomad node drain -enable e144f159
 Are you sure you want to disable drain mode for node "e144f159-0605-fff0-a94b-18a8e238cc19"? [y/N] y
 Before proceeding, keep in mind that:
 2020-04-24T16:52:46+02:00: Ctrl-C to stop monitoring: will not cancel the node drain
 2020-04-24T16:52:46+02:00: Node "e144f159-0605-fff0-a94b-18a8e238cc19" drain strategy set
 2020-04-24T16:52:47+02:00: Alloc "34e84abe-4671-7ec8-dbcb-6ce6f09799ac" marked for migration
 2020-04-24T16:52:47+02:00: Alloc "34e84abe-4671-7ec8-dbcb-6ce6f09799ac" draining
 2020-04-24T16:52:47+02:00: Alloc "e30775e3-2fcb-a022-4631-f0f8bb591afd" marked for migration
 2020-04-24T16:52:47+02:00: Drain complete for node e144f159-0605-fff0-a94b-18a8e238cc19
 2020-04-24T16:52:47+02:00: Alloc "e30775e3-2fcb-a022-4631-f0f8bb591afd" draining
 2020-04-24T16:52:47+02:00: Alloc "34e84abe-4671-7ec8-dbcb-6ce6f09799ac" status running -> complete
 2020-04-24T16:52:53+02:00: Alloc "e30775e3-2fcb-a022-4631-f0f8bb591afd" status running -> complete
 2020-04-24T16:52:53+02:00: All allocations on node "e144f159-0605-fff0-a94b-18a8e238cc19" have stopped
 ```
 - Check the jobs are no longer running on the node (notice the "complete" counter and in the Summary and the complete job in the Allocations on the Node ID that was set in drain state)
 ```
 [root@ceph-consul-testing-91901a3674 ~]# nomad job status httpbin
 ID            = httpbin
 [...]
 
 Summary
 Task Group  Queued  Starting  Running  Failed  Complete  Lost
 httpbin     0       0         2        0       1         0
 [...]
 
 Deployed
 Task Group  Desired  Placed  Healthy  Unhealthy  Progress Deadline
 httpbin     2        2       2        0          2020-04-23T19:49:57+02:00
 
 Allocations
 ID        Node ID   Task Group  Version  Desired  Status    Created     Modified
 a979d763  2fc1eb64  httpbin     1        run      running   12s ago     7s ago
 34e84abe  e144f159  httpbin     1        stop      complete  1h31m ago   11s ago
 7713c865  4cad6b1f  httpbin     1        run      running   22h42m ago  21h13m ago
 ```
 - ssh to the node in drain state and check there are no more Docker container running
 ```
 [root@ceph-consulworker-testing-794c2396aa ~]# docker ps -a
 CONTAINER ID        IMAGE               COMMAND             CREATED             STATUS              PORTS               NAMES
 [root@ceph-consulworker-testing-794c2396aa ~]# 
 ```
  - Upgrade Docker (check the Docker [documentation](https://docs.docker.com/engine/install/centos/))
 ```
 [root@ceph-consulworker-testing-794c2396aa ~]# yum install docker-ce docker-ce-cli containerd.io container-selinux
 [root@ceph-consulworker-testing-794c2396aa ~]# service docker restart
 Redirecting to /bin/systemctl restart docker.service
 [root@ceph-consulworker-testing-794c2396aa ~]# docker version
 Client: Docker Engine - Community
  Version:           19.03.8
  API version:       1.40
 [...]
 ```
 - Put the node back in the pool with nomad
 ```
 [root@ceph-consul-testing-91901a3674 ~]# nomad node drain -disable e144f159
 Are you sure you want to disable drain mode for node "e144f159-0605-fff0-a94b-18a8e238cc19"? [y/N] y
 Node "e144f159-0605-fff0-a94b-18a8e238cc19" drain strategy unset
 ```
 - Check the jobs are schedules and exectuted on the node
 - Put the node back in the load-balanced alias
 ```
 roger update --appstate=production
 ```


### Upgrading rgw with nomad

#### Notes

Should you want to upgrade only a subset of the rgw's to test if the upgrade is disruptive, best is e.g. to do the following (here will directly shows the rgw hosts that runs cbox-backup gateways): 

```bash
ceph-scripts/tools/s3-tools/s3-list-hosts.sh  | grep cbox
```


#### Procedure

 - Clone the [nomad-rgw](https://gitlab.cern.ch/ceph/nomad-rgw) git repository
 - Change the `dummy` value in `rgw-default.hcl` to anything different
 - Get `nomad` tokens to authenticate to the master nodes

```bash
export NOMAD_ADDR=https://ceph-consul-meyrin.cern.ch:4646
export NOMAD_TOKEN=my-little-secret-token
```
- Validate the configuration change using: `nomad plan rgw-default.hcl`
- Then apply the configuration with `nomad run rgw-default.hcl` to proceed to the upgrade
- Watch `nomad job status rgw-default` and, when healthy, go to next step
- Promote the deployment using `nomad job promote rgw-default`

## Basic tasks

### Take a node down for maintenance

There's a hook in `roger` that disables the `radosgw` backend from Consul when the `roger` state of a node is different from `production`.
The `lbalias` client is also configured to evict nodes which state is different from production.

- To remove a node from production: `roger update --appstate intervention --message 'INCXXXX: Reason for intervention' --duration 2h my-little-host`

**Although the `radosgw` backend stops getting traffic in a matter of seconds, the `lbalias` takes ~5 to 10 minutes to apply changes. Be sure to wait or check the DNS alias to ensure the node has been evicted.**

If the implicated node is not reachable, we need to evict it manually from consul:


```bash
[root@ceph-consul-meyrin ~]# consul force-leave cephgabe-rgw-99999.cern.ch
[root@ceph-consul-meyrin ~]# consul members | grep cephgabe-rgw-99999.cern.ch
cephgabe-rgw-99999.cern.ch    [1243:1234:1234:1234::1234]:9999       left    client  1.4.0  2         meyrin  <default>
```
> Note: Do not confuse the previous command with `consul leave` which makes the current node to leave the catalog! 

### Change number of radosgw

- Clone the [nomad-rgw](https://gitlab.cern.ch/ceph/nomad-rgw) git repository
- Change the `count = XX` value in `rgw-default.hcl` for instance
- Get `nomad` tokens to authenticate to the master nodes

```bash
export NOMAD_ADDR=https://ceph-consul-meyrin.cern.ch:4646
export NOMAD_TOKEN=my-little-secret-token
```

- Validate the configuration change

```shell
$ nomad plan rgw-default.hcl
+/- Job: "rgw-default"
+/- Task Group: "radosgw" (5 create, 10 in-place update)
  +/- Count: "10" => "15" (forces create)
      Task: "radosgw"

Scheduler dry-run:
- All tasks successfully allocated.
[...]
```

- Apply the configuration change

```shell
$ nomad run rgw-default.hcl
==> Monitoring evaluation "cf060498"
    Evaluation triggered by job "rgw-default"
[...]
    Evaluation status changed: "pending" -> "complete"
==> Evaluation "cf060498" finished with status "complete"
```

### Check nomad job status

- Using the WebUI
  - Go to: (https://ceph-consul-meyrin.cern.ch:4646)
  - Paste the Nomad token in the `ACL token` page
- Using the CLI
  - Export the following environment variables

```shell
export NOMAD_ADDR=https://ceph-consul-meyrin.cern.ch:4646
export NOMAD_TOKEN=my-little-secret-token
```

  - Run various `nomad` commands

```shell

$ nomad status
ID           Type     Priority  Status   Submit Date
proxy        system   50        running  2018-12-05T22:38:37+01:00
rgw-default  service  60        running  2019-01-29T16:37:59+01:00
$ nomad status rgw-default
ID            = rgw-default
Name          = rgw-default
Submit Date   = 2019-01-29T16:37:59+01:00
Type          = service
Priority      = 60
Datacenters   = meyrin
Status        = running
Periodic      = false
Parameterized = false

Summary
Task Group  Queued  Starting  Running  Failed  Complete  Lost
radosgw     0       0         15       0       0         0

Latest Deployment
ID          = bfa1cf8a
Status      = successful
Description = Deployment completed successfully

Deployed
Task Group  Desired  Placed  Healthy  Unhealthy  Progress Deadline
radosgw     15       15      15       0          2019-01-29T16:49:07+01:00

Allocations
ID        Node ID   Task Group  Version  Desired  Status   Created     Modified
c4874706  dff9541d  radosgw     20       run      running  3m16s ago   2m12s ago
a061bf69  7c1e03ab  radosgw     20       run      running  3m16s ago   2m13s ago
a7fb8473  282f1722  radosgw     20       run      running  3m16s ago   2m8s ago
[...]
```

### Change træfik configuration

One might want to change some parameters of `træfik`, for instance when the TLS certificate is renewed.
Changing the configuration is a bit more involved because `consul` only listens on `localhost` for security reasons.

- Log on a `consul` cluster master: `ssh root@ceph-consul-meyrin.cern.ch -L 8500:localhost:8500`
- Open your browser on (http://localhost:8500): `xdg-open http://localhost:8500`
- Click on `Key/Value` in the menu and navigate the configuration tree to change the desired parameter.

### Change træfik TLS certificate

The certificate is provided by CDA. You should ask them to buy a new one with the correct SANs.
Once the new certificate is provided, copy-paste it on https://tools.keycdn.com/certificate-chain -- It will return a certificate chain with all the required intermediate certificates. The chain can be downloaded and split to check each certificate with `openssl x509 -in <filename> -noout -text`.

Once validate, this should be put into the Consul Key/Value store.
Open a browser, go to `Key/Value` then update `traefik/entrypoints/https/tls/certificates`. Paste in the new certificate.

Next, the træfik containers need to be restarted. To test on one single frontend, put a node in `disabled` state, wait
for it to leave the s3.cern.ch alias, then `docker restart` the relevant container.

Once validated follow the `nomad plan` -> `nomad run` procedure above with `proxy.hcl` to restart all træfik instances.


### Quota alerts

There is a daily cronjob that checks S3 user quota usage and sends a list of accoutns reaching 95% of their quota. Upon reception of this email, we should get in touch with the user and see if they can (1) free some space by deleting unnecessary data or (2) request more space. 

Currently, there is some rgw accounts that will come without an associated email address. A way to investigate who owns the account is to log into `aiadm.cern.ch` and run the following commands (in `/root/ceph-scripts/tools/s3-accounting/`)
```
./cern-get-accounting-unit.sh --id `./s3-user-to-accounting-unit.py <rgw account id>`
```

This will give you the user name of the associated openstack tenant's owner, with the contact email address. 

