# Agile Infrastructure Tools for Ceph

## Setting up the openstack client on aiadm

Login to `aiadm.cern.ch` giving your CERN credentials.

Replace **[user]** with your CERN username

```sh
 $ ssh [user]@aiadm.cern.ch
```

Prepare the environment, so you will be able to see the ceph hosts.

```
[user@aiadmXX ~]$ eval $(ai-rc -s cephadm)
Using default domain (cern.ch) as 'cephadm' does not look like an FQDN
OpenStack environment variables set for Project = 'IT Ceph Storage Service'
[user@aiadmXX ~]$ openstack server list
```

Then you will be able to see the appropriate hosts in your console in the form of a table.

The hosts should start with the prefix `ceph`

| ID                                   | Name                       | Status | Networks                         |
|:------------------------------------:|:--------------------------:|:------:|:--------------------------------:|
| 3cd6336c-6d5b-4a3b-8488-ae2aedd936e9 | ceph-es-master-1a00def965  | ACTIVE | CERN_NETWORK=X.X.X.X, X:X:X:X::X |
| 6dfb37ec-758e-494f-a764-b0dfade16650 | ceph-es-slave-45d66ca0ac   | ACTIVE | CERN_NETWORK=X.X.X.X, X:X:X:X::X |
| ...                                  | ...                        | ...    | ...                              |


## Installation by Cluster

See https://gitlab.cern.ch/ceph/ceph-guide/-/tree/master/src/ops/clusters/
