# Pointers

There are several channels to watch during your Rota shift:

1. Emails to ceph-admins@cern.ch:
2. General informations on clusters (configurations, OSD types, HW, versions): [Instance Version Tracking ticket](https://its.cern.ch/jira/browse/CEPH-558)

# Taking notes

**Each action you take should be noted down in the ticket**, especially information such as OSD ID or the output of command runs. 

# Keeping the Team Informed

If you have any questions or take any significant actions, keep ceph-admins informed.

# Common Procedures

* [scsi_blockdevice_driver_error_reported](#scsi_blockdevice_driver_error_reported)
   * [Draining a Failing OSD](#draining-a-failing-osd)
   * [Creating a new OSD](#creating-a-new-osd-on-a-replacement-disk)

## Reseating of a drive

Please contact ceph-admins before reseating the drive. 


## Update in scripts (June 2020)

Some clusters are now running Ceph Nautilus and will require the use of new scripts for the drain-osd and prepare-for-replacement steps:
* naut-drain-osd.sh
* naut-prepare-for-replacement.sh

They are used exactly the same way as `drain-osd.sh` and `prepare-for-replacement.sh`. 
The idea is to first run the normal script, and if you see the following message: 
```
echo "ceph nautilus detected."
echo "please use './naut-drain-osd.sh --dev <device>' instead"   
```
You need to use the `naut-drain-osd.sh` (or `naut-prepare-for-replacement.sh`) script instead. 
Checks have been added so that it is not possible to execute the wrong script on the wrong cluster. 

## exception.scsi_blockdevice_driver_error_reported

### New procedure for Draining a failing OSD
This section describes the new procedure for handling disk replacements in ceph clusters. The new script `rd-drain-osd.sh` is due to replace and combine both `naut-drain-osd.sh` and `prepare-for-replacement.sh` and used in a similar way. 

0.  `watch ceph status` <- keep this open in a separate window
1.  Login to the machine with a failing drive and open a new `tmux` session (`tmux`).
2.  cd to `ceph-scripts/tools/ceph-disk-replacement/` and run `rd-drain-osd.sh --dev /dev/sdX`. You should see an output of the following form: 
```
[ceph-disk-replacement]# ./rd-drain-osd.sh --dev /dev/<device>
ceph osd out osd.<osd id>;
ceph osd primary-affinity osd.<osd id> 0;
touch /root/log.<cluster>.drain.<host>.cern.ch.<osd id>
while [ `ceph osd df tree --filter_by=name --filter=osd.<osd id> --format=json | jq '.nodes[].pgs'` -ne 0 ]; do
sleep 600; echo "Draining in progress... (`ceph osd df tree --filter_by=name --filter=osd.<osd id> --format=json | jq '.nodes[].pgs'`)";
done;
systemctl stop ceph-osd@<osd id>
if ! `ceph health | grep -q "HEALTH_OK"`
then echo "OSD unsafe to destroy, please contact ceph-admins";
else
umount /var/lib/ceph/osd/ceph-<osd id>
ceph-volume lvm zap --destroy --osd-id <osd id>
touch /root/log.<cluster>.prepare.<host>.cern.ch.<osd id>
rm -f /root/log.<cluster>.drain.<host>.cern.ch.<osd id>
ceph osd destroy <osd id> --yes-i-really-mean-it
fi
```
3.  Run `rd-drain-osd.sh --dev /dev/sdX | sh`. The script will proceed with the drain and print every 10mn the remaining number of PGs attached to the osd. It will then prepare the drive for replacement once the number reaches zero. You can leave the tmux session by using `CTRL+B, D` (and reattach by typing `tmux a`). Here is an example: 
```
[ceph-disk-replacement]# ./rd-drain-osd.sh --dev /dev/<device> | sh
marked out osd.<osd id>.
set osd.<osd id> primary-affinity to 0 (802)
Draining in progress... (31)
Draining in progress... (26)
Draining in progress... (19)
Draining in progress... (15)
Draining in progress... (12)
Draining in progress... (9)
Draining in progress... (3)
```

4. Once the disk has been drained and the script has finished you may physically replace the drive and go ahead with the `recreate-osd.sh` step as usual. What follows is the end of the execution of the script: 
```
Draining in progress... (6)
Draining in progress... (6)
Draining in progress... (6)
Draining in progress... (5)
Draining in progress... (5)
Draining in progress... (4)
Draining in progress... (3)
Draining in progress... (3)
Draining in progress... (2)
Draining in progress... (2)
Draining in progress... (1)
Draining in progress... (0)
--> Zapping: /dev/ceph-8adef34b-11f7-4513-98f3-c9c5f53c21c7/osd-332ec3d8-4eeb-4482-b25c-b949677cf826
Running command: /usr/bin/dd if=/dev/zero of=/dev/ceph-8adef34b-11f7-4513-98f3-c9c5f53c21c7/osd-332ec3d8-4eeb-4482-b25c-b949677cf826 bs=1M count=10 conv=fsync
 stderr: 10+0 records in
10+0 records out
10485760 bytes (10 MB) copied
 stderr: , 0.0687546 s, 153 MB/s
--> Only 1 LV left in VG, will proceed to destroy volume group ceph-8adef34b-11f7-4513-98f3-c9c5f53c21c7
Running command: /usr/sbin/vgremove -v -f ceph-8adef34b-11f7-4513-98f3-c9c5f53c21c7
 stderr: Removing ceph--8adef34b--11f7--4513--98f3--c9c5f53c21c7-osd--332ec3d8--4eeb--4482--b25c--b949677cf826 (253:7)
 stderr: Archiving volume group "ceph-8adef34b-11f7-4513-98f3-c9c5f53c21c7" metadata (seqno 13).
 stderr: Releasing logical volume "osd-332ec3d8-4eeb-4482-b25c-b949677cf826"
 stderr: Creating volume group backup "/etc/lvm/backup/ceph-8adef34b-11f7-4513-98f3-c9c5f53c21c7" (seqno 14).
 stdout: Logical volume "osd-332ec3d8-4eeb-4482-b25c-b949677cf826" successfully removed
 stderr: Removing physical volume "/dev/sdak" from volume group "ceph-8adef34b-11f7-4513-98f3-c9c5f53c21c7"
 stderr: Removing physical volume "/dev/sdal" from volume group "ceph-8adef34b-11f7-4513-98f3-c9c5f53c21c7"
 stdout: Volume group "ceph-8adef34b-11f7-4513-98f3-c9c5f53c21c7" successfully removed
--> Zapping successful for OSD: 598
destroyed osd.598
```

5. Don't forget to end the tmux session (`CTRL+D`, or type `exit`). If the script shows error, or if the tmux session is closed mid-execution, please contact ceph-admins. Also don't forget to copy-paste the log output in the ticket.


### Draining a Failing OSD

This section describes how to prepare a disk to be physically removed.
The scripts needed for the replacement procedure may be found under `ceph-scripts/tools/ceph-disk-replacement/`.

**For failing OSDs in `erin` cluster, check first `facter -p landb_rackname`, machines in racks `EC__ or RA __` are decommissioned, for any other rack, just replace the disk without following the procedure**

*For failing SSDs contact ceph-admins*

*Please note that OSDs in erin/osd/castor uses 2 striped disks, this is handled by the scripts* 

0.  `watch ceph status` <- keep this open in a separate window.

1. Login to the machine with a failing drive and run `./drain-osd.sh --dev /dev/sdX` (the ticket should tell which drive is failing)
   You can also run the following command `ceph-volume lvm list /dev/sdX` to get more ceph-related information about the current drive (e.g. osd id).
   However note that this command might not work on cta/beesly cluster. 
  
   * If the output is of the following form: Take notes of the OSD id `<id>`
   ```
   ceph osd out osd.<id>
   ceph osd primary-affinity osd.<id> 0;
   ``` 
     
   * Else
     * Check for mispelled arguments
     * Ceph might be unhealthy or OSD is unsafe to stop, retry later or contact ceph-admins
     * Else if the script shows a broken output (especially missing `<id>`): Contact ceph-admins

2. Run `./drain-osd.sh --dev /dev/sdX | sh` 
   * Please note that if Ceph already put that osd out, it might say "`osd.<id> is already out.`". You may go ahead and proceed with the next step directly.

3. Once drained (can take a few hours), we now want to prepare the disk for replacement 
   * Run `./prepare-for-replacement.sh --dev /dev/sdX`
   * Continue if the output is of the following form and that the OSD id `<id>` displayed is consistent with what was given by the previous command: 
   ```
   systemctl stop ceph-osd@<id>
   umount /var/lib/ceph/osd/ceph-<id>
   ceph-volume lvm zap /dev/sdX --destroy
   ```
     * (note that the `--destroy` flag will be dropped in case of a FileStore OSD)
     
   * Else
     * If the script shows no output: Ceph is unhealthy or OSD is unsafe to stop, contact ceph-admins
     * Else if the script shows a broken output (especially missing `<id>`): Contact ceph-admins

3. Run `./prepare-for-replacement.sh --dev /dev/sdX | sh` to execute.

4. Now the disk is safe to be physically removed.

5. If there is no replacement drive for the hostgroup ceph/erin/osd, please create a ticket to ceph 3rd level support with the name `ceph/erin no-replace <host> <drive>` and assign it to `Theofilos Mouratidis` if possible.

### Creating a new OSD (on a replacement disk)

When the broken disk has been replaced by a new one, we have to put it back into production.

0.  `watch ceph status` <- keep this open in a separate window.

1. Run `./recreate-osd.sh --dev /dev/sdX` and check that the output is according to the following:
  * On beesly cluster:
  ```
  ceph-volume lvm zap /dev/sdX
  ceph osd destroy <id> --yes-i-really-mean-it
  ceph-volume lvm create --osd-id <id> --data /dev/sdX --block.db /dev/sdY
  ```
  
  * On gabe cluster:
  ```
  ceph-volume lvm zap /dev/sdX
  ceph-volume lvm zap /dev/ceph-block-dbs-[0-9a-f]+/osd-block-db-[0-9a-z-]+
  ceph osd destroy <id> --yes-i-really-mean-it
  ceph-volume lvm create --osd-id <id> --data /dev/sdX --block.db ceph-block-dbs-[0-9a-f]+/osd-block-db-[0-9a-z-]+ 
  ```
 
  * On some erin cluster: 
    ```
    ceph-volume lvm zap /dev/sdX
    ceph osd destroy <id> --yes-i-really-mean-it
    ceph-volume lvm create --osd-id <id> --data /dev/sdX --block.db /dev/sdY
    ```

  * On some erin cluster:
    ```
    ceph osd destroy <id> --yes-i-really-mean-it
    pvremove /dev/sdX
    pvremove /dev/sdY
    pvscan --cache 
    ceph-volume lvm zap /dev/sdX
    ceph-volume lvm zap /dev/sdY
    sleep 1
    pvscan --cache
    sleep 1
    vgcreate ceph-2ef08e23-7a4f-46b3-983d-efcfbdb9de80 /dev/sdX /dev/sdY
    lvcreate -i 2 -l 100%FREE -n osd-f15726b4-5108-4ce8-9535-db4e599030d0 ceph-2ef08e23-7a4f-46b3-983d-efcfbdb9de80
    ceph-volume lvm create --bluestore --data ceph-2ef08e23-7a4f-46b3-983d-efcfbdb9de80/osd-f15726b4-5108-4ce8-9535-db4e599030d0 --osd-id <id>
    ```
    
2. If the output is satisfactory, run `./recreate-osd.sh --dev /dev/sdX | sh`

   
