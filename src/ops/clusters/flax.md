# Installation HDD Capacity August 2020

## Instantiate via Ironic

Creating 16 machines:

```
# export OS_PROJECT_NAME="IT Ceph Ironic"
# ai-bs --landb-mainuser ceph-admins --landb-responsible ceph-admins -f p1.dl8200601.S513-V-IP553 --c8 -g ceph/spare --prefix cephdata20b-
```

## Reinstall with ai-installhost

This is to get a RAID1 EFI-compatible boot disk:

Reinstall all 16 machines with:
```
# ai-foreman updatehost -p 'Ceph (EFI)' -m CentOS8 <hostname>
# ai-installhost --mode uefi --aims-kopts "modprobe.blacklist=mpt2sas,mpt3sas inst.sshd" <hostname>
```

