### ceph/vault

#### Install first time:

```
ai-foreman updatehost -p 'Ceph (EFI)' -o 'CentOS 7.7' <hostname>
ai-installhost --mode uefi --aims-kopts "modprobe.blacklist=mpt2sas,mpt3sas inst.sshd" <hostname>
```

#### To reinstall OS:

```
ai-installhost --mode uefi --aims-kopts "modprobe.blacklist=mpt2sas,mpt3sas inst.sshd" <hostname>
```

After the host is booted, run puppet twice then activate the osds:
```
puppet agent -t
puppet agent -t
ceph-volume lvm activate --all
```
