### ceph/nethub


#### New HW deliveries Summer 2021


-----


#### Reinstall old HW on CentOS 7
```
ai-foreman updatehost -p 'Ceph (XFS)' -o 'CentOS 7.9' -m 'CentOS mirror'  cephnethub-data-<node_name>
ai-installhost --mode bios --aims-kopts "modprobe.blacklist=mpt2sas inst.sshd" cephnethub-data-<node_name>

eval $(ai-rc "IT Ceph Ironic")
openstack console url show cephnethub-data-<node_name> -f json
# Run the `ipmitool_mc_reset_cold` of the output above
ipmitool -U ...

openstack server reboot --hard cephnethub-data-<node_name>
```

#### Reinstall old HW on CentOS Stream 8

**Warning** -- This has been a failure so far!

- C8 Stream does not detect SSD devices attached to the motherboard but only the JBOD through the SAS controller.
- Such drives come with Intel Rapid Storage Technology, which requires dedicated drivers not ported to CentOS Stream 8.

- The following has been tested with no luck:
```
ai-foreman updatehost -p 'Ceph (XFS)' -o 'CentOS Stream 8' -m CentOSStream8 cephnethub-data-<node_name>
ai-installhost --mode bios --aims-kopts "modprobe.blacklist=mpt2sas,mpt3sas inst.sshd" cephnethub-data-<node_name>

eval $(ai-rc "IT Ceph Ironic")
openstack console url show cephnethub-data-<node_name> -f json
# Run the `ipmitool_mc_reset_cold` of the output above
ipmitool -U ...

openstack server reboot --hard cephnethub-data-<node_name>
```

