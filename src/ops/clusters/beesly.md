### ceph/beesly

#### New HW deliveries January 2021

#### Install first time:

- Create the machine in OpenStack Ironic
  ```
  unset OS_PROJECT_ID;
  unset OS_TENANT_ID;
  unset OS_TENANT_NAME;
  export OS_PROJECT_NAME="IT Ceph Ironic";
  
  FLAVOR='p1.dl8330179.S513-C-IP501'
  PREFIX='cephdata21a-'
  
  ai-bs     --landb-mainuser ceph-admins \
            --landb-responsible ceph-admins \
            --nova-flavor $FLAVOR \
            --c8 \
            --foreman-environment 'production' \
            --foreman-hostgroup 'ceph/spare' \
            --nova-sshkey ebocchi \
            --prefix $PREFIX
  ```

- Reinstall with UEFI and software-mirrored (md-raid) system disk
  ```
  ai-foreman updatehost -p 'Ceph (EFI)' -o 'CentOS Stream 8' -m CentOS8Stream cephdata21a-<node_name>
  ai-installhost --mode uefi --aims-kopts "inst.sshd" cephdata21a-<node_name>
  openstack server reboot --hard cephdata21a-<node_name>
  ```

**Warning**:
We have seen a spurios partition on one of the two systems disks being created while installing.
The software raid was not complaining but the begin/end sectors of each partition were different on the two disks.
Reinstalling the machine a second time fixes the problem.

Always check with `lsblk`, `parted`, and `gdisk` the the system disks are partitioned correctly before proceeding with the configuration of the server.

