# MCO Tips

When updating this cluster you can filter per "pod" like this:

```
mco find -T cloud_compute -F hostgroup=cloud_compute/level2/main/crit_project_004 -F landb_serial_number=/DL7873990-253883/ --dt=3
```
