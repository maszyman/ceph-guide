## Ceph Clusters Review Procedure

### Cluster Overview

| Cluster  | Lead   | Use-case                                          | Mon host      | Release  | Version    | Racks                                   | IP Services                                                     | Power                                | SSB Upgrades?      |
| -------- | ------ | ------------------------------------------------- | ------------- | -------- | ---------- | --------------------------------------- | --------------------------------------------------------------- | ------------------------------------ | ------------------ |
| barn     | Enrico | Cinder:<br/> barn-100, barn-500                   | cephbarn      | octopus  | 15.2.15-2  | BA09                                    | S513-A-IP250	                                            | UPS-4/-C                             | Yes                |
| beesly   | Enrico | Glance<br/>Cinder:<br/> standard, io1, cp1, cpio1 | cephmon       | octopus | 15.2.14-7  | RA01-RA21<br/>(CD27-CD30)<br/>BA09-BA12 | S513-C-IP750<br/>S513-C-IP751<br/>S513-A-IP38<br/>S513-A-IP63   | UPS-4<br/>(UPS-3)<br/>UPS-4/-C       | Yes                |
| cta      | Theo   | CTA                                               | cephcta       | octopus  | 15.2.14-0  | SI36-SI41                               | -                                                               |                                      | No, Steve Murray   |
| dwight   | Dan    | Testing+Manila                                    | cephmond      | octopus  | 15.2.15-2  | RA13,RA17,RA21                          | S513-C-IP729<br/>S513-C-730                                     |                                      | Yes + Manila MM    |
| flax     | Dan    | HPC+Manila                                        | cephflax      | nautilus | 14.2.22-1  | BA10,SQ05<br/>CQ18-CQ21<br/>SJ04-SJ07   | S513-A-IP558,S513-V-IP562<br/>S513-C-IP164<br/>S513-V-IP553     | UPS-4/-C,UPS-1<br/>UPS-1 <br/>UPS-3  | Yes                |
| gabe     | Enrico | S3                                                | cephgabe      | nautilus | 14.2.21-3  | SE04-SE07<br/>SJ04-SJ07                 | S513-V-IP808<br/>S513-V-IP553                                   | UPS-1<br/>UPS-3                      | Yes                |
| jim      | Dan    | HPC BE                                            | cephjim       | nautilus | 14.2.22-1  | SW11-SW15<br/>SX11-SX15                 | S513-V-IP194<br/>S513-V-IP193                                   | UPS-3<br/>UPS-3                      | No, Pablo Llopis   |
| kelly    | Theo   | Kopano + CTA prod db                              | cephkelly     | octopus  | 15.2.14-7  | CQ12-CQ22                               | -                                                               |                                      | Yes + Steve        |
| kopano   | Dan    | Dovecot                                           | cephkopano    | nautilus | 14.2.22-1  | BE10 BE11 BE13                          | S513-A-IP22                                                     | UPS-4/-C                             | Yes + Giacomo      |
| levinson | Arthur | CephFS SSD                                        | cephlevinson  | octopus | 15.2.14-7  | SE04-12<br/>SF05-14                     | S513-V-IP808<br/>S513-V-IP808                                   | UPS-1<br/>UPS-1                      | Yes                |
| meredith | Enrico | Cinder: io2, io3                                  | cephmeredith  | octopus | 15.2.14-7  | CK01-23                                 | S513-C-IP562                                                    | UPS-2                                | Yes                |
| nethub   | Enrico | S3 FR                                             | cephnethub    | nautilus | 14.2.22-1  | HA06-HA09<br/>HB01-HB07                 | S773-C-IP100,S773-C-IP180<br/>S773-C-IP101,S773-C-IP102         | EOD1/0E,ESD4/0E<br/>EOD105/0E        | Yes                |
| octopus  | Theo   | Testing                                           | cephoctopus-1 | octopus  | 15.2.15-2  | -                                       |                                                                 |                                      | No                 |
| next     | Theo   | Reva Testing                                      | cephnext01    | pacific  | 16.2.4-0   | -                                       |                                                                 |                                      | No                 |
| pam      | Arthur | CephFS                                            | cephpam       | octopus  | 16.2.5-0   | SJ04-SJ07                               | S513-V-IP553                                                    | UPS-3                                | No                 |
| ryan     | Enrico | Cinder: 3rd AZ                                    | cephryan      | octopus  | 15.2.15-2  | CE01-CE03                               | S513-C-IP501                                                    | UPS-2                                | Yes                |
| vault    | Enrico | Cinder: vault-100, vault-500                      | cephvault     | octopus | 15.2.14-7  | SE04-SE07                               | S513-V-IP808                                                    | UPS-1                                | Yes                |

Each production cluster has a designated *cluster lead*, who is the primary contact and responsible for that cluster.

The user-visible "services" provided by the clusters are documented in our Service Availability probe: https://gitlab.cern.ch/ai/it-puppet-hostgroup-ceph/-/blob/qa/code/files/sls/ceph-availability-producer.py#L19

The QoS provided by each user-visible cluster is describe in OpenStack docs: https://clouddocs.web.cern.ch/details/block_volumes.html

### Reviewing a Cluster Status

1. Check Grafana dashboards for unusual activity, patterns, memory usage:
* https://filer-carbon.cern.ch/grafana/d/000000001/ceph-dashboard
* https://filer-carbon.cern.ch/grafana/d/000000108/ceph-osd-mempools
* https://filer-carbon.cern.ch/grafana/d/uHevna1Mk/ceph-hosts
* For RGWs: https://filer-carbon.cern.ch/grafana/d/iyLKxjoGk/s3-rgw-perf-dumps
* For CephFS: * https://filer-carbon.cern.ch/grafana/d/000000111/cephfs-detail
* etc...

2. Login to cluster mon and check various things:
* `ceph osd pool ls detail` - are the pool flags correct? e.g. `nodelete,nopgchange,nosizechange`
* `ceph df` - assess amount of free space for capacity planning
* `ceph osd crush rule ls`, `ceph osd crush rule dump` - are the crush rules as expected?
* `ceph balancer status` - as expected? 
* `ceph osd df tree` - are the PGs per OSD balanced and a reasonable number, e.g. < 100.
* `ceph osd tree out`, `ceph osd tree down` - are there any OSDs that are not being replaced properly?
* `ceph config dump` - is the configuration as expected?
* `ceph telemetry status` - check from config if it on, enable it

### Hardware Specs
* [Ceph clusters' block devices specs](https://its.cern.ch/jira/browse/CEPH-1251)
