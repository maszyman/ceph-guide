# S3 Scripts

Under `ceph-scripts/tools/s3-*` are living some S3 tools.

## s3-accounting

Collection of scripts needed for the accounting of the S3 services (between others). 

### `get-s3-accounting.sh`
This is the script that collects s3 accounting data and feed them to the dedicated [webpage](https://storage.web.cern.ch/storage/accounting/).
* Dependencies: `cern-get-accounting-unit.sh`, `get-s3-user-stats.py`, `s3-user-to-accounting-unit.py`, S3 service (`s3://s3-accounting-files`) and access to `/eos/project/f/fdo/www/accounting/`.
* Used by: running as a cronjob on aiadm

### `cern-get-accounting-unit.sh`
This gives accounting info about an account/email: 
* Dependencies: None
* Used by: `get-s3-accounting.sh`

  ```
  $> ./cern-get-accounting-unit.sh --id jcollet -f
  jcollet, julien.collet@cern.ch, IT/ST/GSS
  $> ./cern-get-accounting-unit.sh --id jcollet
  jcollet, julien.collet@cern.ch
  $> ./cern-get-accounting-unit.sh --id julien.collet@cern.ch
  jcollet, julien.collet@cern.ch
  ```

### `get-s3-user-stats.py` 
This gets the S3 user usage detailled stats. For each user, get the stats if they have quota enabled (projects) and compute the percentage used.
* Dependencies: None
* Used by: `get-s3-accounting.sh`

### `s3-user-to-accounting-unit.py` 
This translate the S3 uuid to its owner through some Openstack magic.
* Dependencies: Access to openstack, `clouds.yaml` and the `Services` account
* Used by: `get-s3-accounting.sh`

  ```
  $> ./s3-user-to-accounting-unit.py <project id>
  <user id> 
  ```

## s3-scanner 


### Cronjob info: 
Periodically scans S3 buckets and checks for bucket publicly open. Script in production, every day scans for publicly available buckets, except if bucket name matches one of the name in the blacklist (see s3://s3-scanner/blacklist, list can be updated at will). Output is in the file under s3://s3-scanner/scan-output and is updated every day.
Output format is JSON:
    
    ```
    $> cat scan-output | jq .
    { "<owner account>": [ "<email>@cern.ch: <bucket name>", "<email>@cern.ch: <bucket name>", .... ] }
    ```

* Dependencies: Access to cephgabe for full bucket list (if no input provided), access to `s3://s3-scanner/`
* Used by: running as a cronjob on aiadm

### Standalone mode:

Options: 
```
$> python3 ./cern-s3-scanner.py --help
  -h, --help            show this help message and exit
  -o OUTFILE, --out-file OUTFILE
                        Output file, if unset, will output to terminal

  -b BLACKLIST, --black-list BLACKLIST
                        File containing a list of pattern to black list entries in the list
                        Set to s3://s3-scanner/blacklist for the script to use the file stored on S3

  -m MODE, --mode MODE  Scan mode: instead of printing only publicly
                        accessible resources: 
                           - listall, dump everything 
                           - listopen, dump open buckets and their contents 
                           - bucketonly, print only bucket, not their content

  -i BUCKETS, --input BUCKETS
                        Name of text file containing buckets to check
                        If left empty, will ask gabe for the list of all buckets

```

## s3-tools

### `s3-list-hosts.sh`
Little utility to match rgw hosts to their job. 
```
$> ./s3-list-hosts.sh
rgw-atlas cephgabe-rgwxl-f250ed5924
rgw-gitlab cephgabe-rgwxl-1800367aec
rgw-cbox cephgabe-rgwxl-3c81b5e0c3
rgw-default cephgabe-rgwxl-5a4b412a86
rgw-atlas cephgabe-rgwxl-56f5a42d0c
spare cephgabe-rgwxl-bf50c609d1
rgw-gitlab cephgabe-rgwxl-fabc864860
rgw-cvmfs cephgabe-rgwxl-3e1a55e60a
rgw-cbox cephgabe-rgwxl-58511415b3
rgw-cvmfs cephgabe-rgwxl-d8a2425079
rgw-atlas cephgabe-rgwxl-1d701ccbee
rgw-default cephgabe-rgwxl-044ed0bdd2
rgw-default cephgabe-rgwxl-a926962c19
rgw-gitlab cephgabe-rgwxl-315091d343
rgw-default cephgabe-rgwxl-86c0367dce
```
* Dependencies: `s3-radosgw-whoami.sh`
* Used by: None

### `s3-radosgw-whoami.sh`
Shows the rgw job executed on the host

```
$> ssh -T `host s3.cern.ch | head -n 1 | grep -Eo "[0-9]+.[0-9]+.[0-9]+.[0-9]+"` < ./s3-radosgw-whoami.sh
rgw-cvmfs
```

* Dependencies: None
* Used by: `s3-list-hosts.sh`

## S3 WARP Benchmarking
WARP is a benchmarking too for S3 ([Git repo](https://github.com/minio/warp)). 
The VM `warp-s3-benchmark` is running two scripts (`run-benchmark.sh` and `run-benchmark-2.sh`) in a cron to periodically send performance measurements of gabe and nethub to a dashboard ([Ceph S3 dashboard](https://filer-carbon.cern.ch/grafana/d/3lH_g5ziz/ceph-s3-dashboard?orgId=1)).






