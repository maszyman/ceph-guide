## Ceph Ops Documentation

This repo documents the operations procedures for Ceph at CERN.

See [clusters](clusters/) for individual cluster docs.

Upstream Docs are preferred over any local interpretations:
* https://docs.ceph.com/en/latest/

Puppet modules we maintain:
* https://gitlab.cern.ch/ai/it-puppet-hostgroup-ceph
* https://gitlab.cern.ch/ai/it-puppet-module-ceph
* https://gitlab.cern.ch/ai/it-puppet-module-cephfs

Tools we use in production:
* https://gitlab.cern.ch/ceph/ceph-scripts/

Other links:
* Monitoring:
   * [Grafana Dashboard](https://filer-carbon.cern.ch/grafana/dashboard/db/ceph-dashboard)
   * [SSD Endurance](https://monit-grafana.cern.ch/d/ZGRRYrTZz/ceph-ssd-endurance?orgId=49)
   * [S3 ES](https://es-ceph.cern.ch)
   * [Upstream Community Telemetry](https://telemetry-public.ceph.com/)
* Service Now: https://cern.service-now.com/service-portal?id=service_element&name=Ceph-Service
* Mattermost: https://mattermost.web.cern.ch/it-dep/channels/ceph-internal
* SSB: https://cern.service-now.com/service-portal?id=service_status_board
* Availability: https://cern.service-now.com/service-portal?id=service_availability
