# Service Now

## Table of Contents

  * [exception.high_load](#high-load)
  * [exception.nonwriteable_filesystems](#non-writeable-filesystems)
  * [exception.YUM_error](#yum-error)
  * [exception.puppetd_wrong](#puppetd-wrong)
  * [disks in a bad shape (osd abrt crash emails)](#disks-in-a-bad-shape)

### High Load

**Incident example**: [INC1494483](https://cern.service-now.com/service-portal/view-incident.do?n=INC1494483)

There are tickets in [service now](https://cern.service-now.com) with the title:
`[GNI] exception.high_load in ceph/<cluster>/<mon|mds|osd>`
This means the average load of nodes in this hostgroup has surpassed a certain limit
, in `ceph/erin/osd` it is 192.

The message would look like this:
```
24-10-2017 14:31:42 - GNI Web interfaceAdditional comments (Customer View)
Exceptions: exception.high_load
Entities: p05972678u41530
No troubleshooting information
Correlation expression: p05972678u41530:20002:1[236.98]_>_192
Essential machines: YES
Hostgroup: ceph/erin/osd
```

The `p05972678u41530` has an average load of **236** at that time, when it
should be lower than **192**. The numbers indicate the amount of cores needed to
process the current tasks, in `ceph/erin` the osds have 32 cores, so if the
average load is more than 32, the processes start to run slower (process scheduling)

The first thing to do is to `ssh` to the affected machines, defined in the
entities key, and run the command `uptime`, and you will get something like this:
```
[12:02][root@p05972678u41530 (production:ceph/erin/osd*29) ~]# uptime
12:03:04 up 494 days, 18:26, 1 user, load average: 8.32, 7.89, 7.11
```

The numbers are the load averages of the last minute, 5 minutes and 15 minutes
respectively, so you have a general idea of what is happening.

**\*IF\*** these numbers are below the threshold, the ticket can be closed with
a message containing the result of `uptime` and a text after saying that the load
of the server is normal now.

**\*IF\*** The number doesn't become lower within about 30 minutes, you will have
to go to **Dan Van Der Ster** to investigate the problem

### Non-writeable filesystems

**Incident example**: [INC1499826](https://cern.service-now.com/service-portal/view-incident.do?n=INC1499826)

There are tickets in [service now](https://cern.service-now.com) with the title:
`[GNI] exception.nonwriteable_filesystems in ceph/<cluster>/osd`

The message would look like this:
```
Exceptions: exception.nonwriteable_filesystems
Entities: p05972678b41181
No troubleshooting information
Correlation expression: ((p05972678b41181:9104:3[rw,seclabel,noatime,attr2,inode64,logbufs=8,logbsize=256k,noquota]_regex_'rw')_&&_(p05972678b41181:9104:9[0]_!=_1)_&&_(p05972678b41181:9104:2[xfs]_ne_'nfs')_&&_(p05972678b41181:9104:1[/var/lib/ceph/osd/ceph-520]_ne_'the_mountpoint'))
Essential machines: YES
Hostgroup: ceph/erin/osd
```

The first thing we do is to search if this issue is fixed in another ticket, by
searching tickets related to this incident. In our case the issue was fixed by
detecting and replacing a failed drive in the machine in some other tickets. If
that's your case, you can close the issue by formatting and preparing the disk and
add it to the cluster. If this is not the case, you need to go to **Dan Van Der Ster**
to investigate this issue.

### YUM error

**Incident example**: [INC1508473](https://cern.service-now.com/service-portal/view-incident.do?n=INC1508473)

The following message
```
Exceptions: exception.YUM_error
Entities: cephironic-mds-c4fbd7ee74
No troubleshooting information
Correlation expression: cephironic-mds-c4fbd7ee74:13034:1[1]_!=_0
Essential machines: No
Hostgroup: ceph/ironic/mds
```
is seen when there is a problem with `yum distro-sync` cron job that runs on the
servers. You will have to investigate the issue in `/var/log/distro_sync.log`. It
usually happens when the sync can't reach some repo due to a temporary network
problem. If there are no problems in the log file or by running the command `/usr/local/sbin/distro_sync.sh`
(it is run as a cron job, run `crontab -l | grep sync` to find it)
you can close the ticket with the output message of the yum sync. In our case the
new release of luminous added a new package `python-rgw` that caused the problem.
To resolve the issue we have to add the exception to the `osrepos_sync_exclude_packages`
field in the `it-puppet-hostgroup-ceph/data/hostgroup/ceph.yaml` file and create
a merge request to "qa".

### Puppetd wrong

**Incident example**: [INC1512519](https://cern.service-now.com/service-portal/view-incident.do?n=INC1512519)

This should be the message template:
```
11-11-2017 06:53:58 - GNI Web interfaceAdditional comments (Customer View)
Exceptions: exception.puppetd_wrong
Entities: p05972678w64745
No troubleshooting information
Correlation expression: ((p05972678w64745:13001:1[88186]_>_86400)_&&_(p05972678w64745:9001:1[43148829]_>_10800))
Essential machines: YES
Hostgroup: ceph/erin/osd
```

Check the host on [foreman](https://judy.cern.ch) if it has any errors in the last runs.
This usually happens when there is a temporary error in the puppet servers. If everything
is fine on foreman, you can close the ticket with the results of some of the last runs
like the message on the incident example.

### Disks in a bad shape

For emails with this subject, repeated over time, with same hostname and osd id, could be caused by a failing disk.

`p05151113781242.cern.ch [ceph/erin/osd] abrt crash report for /usr/bin/ceph-osd [ceph-osd-12.2.4-0.el7]`

- Identify the machine and problematic osd (you can get it from the email subject/body)

```
cmdline:        /usr/bin/ceph-osd -f --cluster ceph --id XXX --setuser ceph --setgroup ceph
```

- Log into that node and check for smart errors on the disk where the osd resides.

```
$ mount
/dev/sdi1 on /var/lib/ceph/osd/ceph-XXX type xfs (rw,noatime,seclabel,attr2,inode64,logbufs=8,logbsize=256k,noquota)
$ smartcl --all /dev/sdi
```

- If you find recent errors most probably the disk is not fine. You need to stop the osd set it out of the cluster

```
$ systemctl stop ceph-osd@XXX.service
$ ceph osd out XXX
```

- This will cause the cluster to go *HEALTH_WARN* status for some time while ceph recreates in another place the objects which reside in that osd.
- No more action needed. The repair service will replace the faulty disk and when done, we need to configure the osd on it.
