# Cluster Known Issues

## beesly

* Used for OpenStack Cinder volumes (standard, io1, cp1, cpio1 volume types)
* Outages on this cluster are a top priority
* Network issues:
   * recent issues with the barn network:
      * critical-power pool may have PGs go `stale` or `unknown` during a network outage.
      * in case of OSDs flapping, set `noout` `nodown` and contact Vincent Ducret
* inconsistent PGs do not auto repair (because may OSDs are still FileStore).
   * ssh to `cephmon.cern.ch` and run `/root/ceph-scripts/tools/scrubbing/autorepair.sh`

## CTA

* Used for CTA testing
* Main user contact: Julien Leduc

## dwight

* Used for Openstack Manila (cephfs) testing volumes

## erin

* Main user contact: Eric Cano
* Used for CASTOR

## flax

* Used for CephFS (OpenStack Manila) and HPC home directories
* Flax has several active MDS's and RAM usage during recovery has been a recent problem.
   * If an active MDS fails and the standby fails to become active, this may help: https://its.cern.ch/jira/projects/CEPH/issues/CEPH-884
   * Otherwise, create an SSB and contact Dan.

* Warnings about Client failed to release caps:
   * These are usually harmless -- client will be evicted if it does not release caps for 900 seconds.

## gabe

* Used for S3.CERN.CH
* Contact for quota or credential sync problems: Jose Castro Leon

## jim

* Main user contact: Pablo Llopis
* Used for HPC CephFS

## kelly

* Main user contact for CephFS: Giacomo Tenaglia
* Main user contact for CTA: Julien Leduc
* The MDS can go out of memory while starting due to openfiles table.
   * `ceph fs status`, `ceph status`, and `top` on the mds machines can help identify this issue
   * if `fs is degraded` and `top` shows the `ceph-mds` process is running out of memory during the `rejoin` step, then you can do the following to remove the openfiles table object:
      * `rados -p cephfs_metadata_pool rm mds0_openfiles.0`

## kopano

* Main user contact: Giacomo Tenaglia
* Used for Dovecot mail testing.

## vault

* Main user contact: Jose Castro Leon, Arne Wiebalck
* Used for Openstack Cinder block storage (vault-100, vault-500 volume types)

## nethub

* Used for S3-FR-PREVESSIN-1.cern.ch
