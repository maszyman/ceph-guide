# Building Releases in Koji

## Links:

* https://linux.web.cern.ch/koji/ -- Intro to Koji @ CERN
* https://koji.cern.ch/ -- the Koji dashboard

## Ceph Tags and Repos

`ceph7` and `ceph8` are used by anything using it-puppet-module-ceph (e.g. our clusters, some clients)
`ceph-client7` and `ceph-client8` are used by it-puppet-module-cephfs clients.

* `ceph7` ([stable](http://linuxsoft.cern.ch/internal/repos/ceph7-stable/), [qa](http://linuxsoft.cern.ch/internal/repos/ceph7-qa/), [testing](http://linuxsoft.cern.ch/internal/repos/ceph7-testing/))
* `ceph8` ([stable](http://linuxsoft.cern.ch/internal/repos/ceph8-stable/), [qa](http://linuxsoft.cern.ch/internal/repos/ceph8-qa/), [testing](http://linuxsoft.cern.ch/internal/repos/ceph8-testing/))
* `ceph8s` ([stable](http://linuxsoft.cern.ch/internal/repos/ceph8s-stable/), [qa](http://linuxsoft.cern.ch/internal/repos/ceph8s-qa/), [testing](http://linuxsoft.cern.ch/internal/repos/ceph8s-testing/))
* `ceph-client7` ([stable](http://linuxsoft.cern.ch/internal/repos/ceph-client7-stable/), [qa](http://linuxsoft.cern.ch/internal/repos/ceph-client7-qa/), [testing](http://linuxsoft.cern.ch/internal/repos/ceph-client7-testing/))
* `ceph-client8` ([stable](http://linuxsoft.cern.ch/internal/repos/ceph-client8-stable/), [qa](http://linuxsoft.cern.ch/internal/repos/ceph-client8-qa/), [testing](http://linuxsoft.cern.ch/internal/repos/ceph-client8-testing/))
* `ceph-client8s` ([stable](http://linuxsoft.cern.ch/internal/repos/ceph-client8s-stable/), [qa](http://linuxsoft.cern.ch/internal/repos/ceph-client8s-qa/), [testing](http://linuxsoft.cern.ch/internal/repos/ceph-client8s-testing/))

## Building a Release

1. Login to aiadm.
2. Download the src.rpm from download.ceph.com
3. `koji build ceph-client8 ceph-14.2.16-0.el8.src.rpm`

### Troubleshooting Builds.

* As of Dec 2020:
   * el8 builds compile without problems.
   * el7 builds require a patch to the spec file, e.g.

```
> diff -u ceph.spec ceph-14.2.16.el7.spec
--- ceph.spec	2020-12-16 18:44:56.000000001 +0100
+++ ceph-14.2.16.el7.spec	2020-12-17 16:36:10.000000001 +0100
@@ -92,9 +92,9 @@

 %{!?_udevrulesdir: %global _udevrulesdir /lib/udev/rules.d}
 %{!?tmpfiles_create: %global tmpfiles_create systemd-tmpfiles --create}
-%{!?python3_pkgversion: %global python3_pkgversion 3}
+%{!?python3_pkgversion: %global python3_pkgversion 36}
 %{!?python3_version_nodots: %global python3_version_nodots 3}
-%{!?python3_version: %global python3_version 3}
+%{!?python3_version: %global python3_version 36}
 # define _python_buildid macro which will expand to the empty string when
 # building with python2
 %global _python_buildid %{?_defined_if_python2_absent:%{python3_pkgversion}}
@@ -247,7 +247,7 @@
 #BuildRequires:  krb5-devel
 BuildRequires:  cunit-devel
 BuildRequires:	python%{_python_buildid}-setuptools
-BuildRequires:	python%{_python_buildid}-Cython
+BuildRequires:	python36-Cython
 BuildRequires:	python%{_python_buildid}-PrettyTable
 BuildRequires:	python%{_python_buildid}-Sphinx
 BuildRequires:  rdma-core-devel
@@ -272,11 +272,7 @@
 %endif
 BuildRequires:	python%{python3_pkgversion}-devel
 BuildRequires:	python%{python3_pkgversion}-setuptools
-%if 0%{?rhel} == 7
-BuildRequires:	python%{python3_version_nodots}-Cython
-%else
-BuildRequires:	python%{python3_pkgversion}-Cython
-%endif
+BuildRequires:	python36-Cython
 BuildRequires:	python%{_python_buildid}-prettytable
 BuildRequires:	python%{_python_buildid}-sphinx
 BuildRequires:	lz4-devel >= 1.7
```


## Building a Patched Release

1. Login to aiadm
2. Download a src.rpm from download.ceph.com
3. Install it: `rpm -ivh ceph-14.2.16-0.el8.src.rpm`
4. Acquire or create patches, e.g.
   * From an upstream PR: https://patch-diff.githubusercontent.com/raw/ceph/ceph/pull/38637.patch
   * From a local cherry-pick:
```
git checkout v14.2.16
git cherry-pick <sha1>
git format-patch v14.2.16
```
   * The patch files need to be copied to ~/rpmbuild/SOURCES/
5. Patch the spec file as needed, e.g.
```
--- ceph-14.2.16.el8.spec	2020-12-17 14:02:01.000000001 +0100
+++ ceph.spec	2020-12-18 09:54:43.000000001 +0100
@@ -110,7 +110,7 @@
 #################################################################################
 Name:		ceph
 Version:	14.2.16
-Release:	0%{?dist}
+Release:	1%{?dist}
 %if 0%{?fedora} || 0%{?rhel}
 Epoch:		2
 %endif
@@ -126,6 +126,9 @@
 %endif
 URL:		http://ceph.com/
 Source0:	%{?_remote_tarball_prefix}ceph-14.2.16.tar.bz2
+Patch0:         36909.patch
+Patch1:         36982.patch
+Patch2:         0001-mds-account-for-closing-sessions-in-hit_session.patch
 %if 0%{?suse_version}
 # _insert_obs_source_lines_here
 ExclusiveArch:  x86_64 aarch64 ppc64le s390x
```
6. Build a new src rpm, e.g. `rpmbuild -bs rpmbuild/SPECS/ceph.spec`
7. Build the rpm, e.g. `koji build ceph-client8 ~/rpmbuild/SRPMS/ceph-14.2.16-1.el8.src.rpm`

## Tagging Releases

The above procedures will create binary rpms in the `*-testing` repos. When they are ready for release, you can tag to qa/stable following the CRM procedure.

* For server repos, we tag to qa/stable according to our operational needs/schedule.
* For client repos, we must follow the 7-day qa cycle, e.g.
   * Create an IT CRM: https://its.cern.ch/jira/projects/CRM/issues
   * Tag to qa: `koji tag-build ceph-client8-qa ceph-14.2.16-0.el8`
   * Click `Merged into QA`
   * Wait 7 days
   * Tag to stable: `koji tag-build ceph-client8-stable ceph-14.2.16-0.el8`
   * Click `Merged into Production`
