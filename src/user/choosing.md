# Which Storage Service is Right for Me?

I need an extra drive for my OpenStack VM:
* See [block storage](rbd.html)

I need a POSIX filesystem shared across a small number of servers:
* See [CephFS via Manila](cephfs.html)

I need storage which is accessible from lxplus, lxbatch, or the WLCG:
* See [EOS](https://cern.service-now.com/service-portal/service-element.do?name=eos-service)

I need to share my files or collaborate with colleages:
* See [CERNbox](https://cern.service-now.com/service-portal/service-element.do?name=CERNBox-Service)

I need HTTP accessible cloud storage for my application:
* See [S3](s3.html)

I need to distribute static software or data globally:
* See [CVMFS](https://cern.service-now.com/service-portal/service-element.do?name=cvmfs)

I need to archive data to tape:
* See [CASTOR](https://cern.service-now.com/service-portal/service-element.do?name=castor-service)
