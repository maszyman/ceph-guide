# Using S3 or SWIFT

S3/Swift is made available via OpenStack. 
See [https://clouddocs.web.cern.ch/object_store/README.html](https://clouddocs.web.cern.ch/object_store/README.html) for more info.
