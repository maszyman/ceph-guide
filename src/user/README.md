# User's Guide

This chapter provides minimal Ceph user documentation.

Block Storage and CephFS documentation is available at the following URLs:
 * [https://clouddocs.web.cern.ch/clouddocs/details/volumes.html](https://clouddocs.web.cern.ch/clouddocs/details/volumes.html)
 * [https://clouddocs.web.cern.ch/clouddocs/file_shares/index.html](https://clouddocs.web.cern.ch/clouddocs/file_shares/index.html)
