# Using Block Storage

Block storage is accessible via OpenStack VMs as documented here: [https://clouddocs.web.cern.ch/clouddocs/details/volumes.html](https://clouddocs.web.cern.ch/clouddocs/details/volumes.html)
