# Using CephFS

CephFS is made available via OpenStack Manila. See [https://clouddocs.web.cern.ch/clouddocs/file_shares/index.html](https://clouddocs.web.cern.ch/clouddocs/file_shares/index.html) for more info.
