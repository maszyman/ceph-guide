# Configure aws cli 

The aws s3api is useful for doing advanced s3 operations, e.g. dealing with object versions.
The following explains how to set this up with our s3.cern.ch endpoint. 


## Setting up aws

All of the information required to set up aws-cli can be found in the existing .s3cfg file used when using S3. 


```
$> yum install awscli 
$> aws configure
AWS Access Key ID [None]: <your access key> 
AWS Secret Access Key [None]: <your secret key>
Default region name [None]:
Default output format [None]:
``` 

## Testing

```
$> aws --endpoint-url=http://s3.cern.ch s3api list-buckets
{
  "Buckets": [
     {
         "Name": <bucket1>,
         "CreationDate": <timestamp> 
     },
     {
       ....
     }
   ],
   "Owner": {
        "DisplayName": <owner>,
        "ID": <owner id>
    }

}
```

# Delete all object versions

We provide [here](https://s3utilities.s3.cern.ch/s3-delete-all-object-versions.sh) a script to help user make sure all versions of their objects are deleted. 

Usage: 
```
$> ./s3-delete-all-object-versions.sh -b <bucket> [-f]
   -b: bucket name to be cleaned up
   -f: if omitted, the script will simply display a summary of actions. Add -f to execute them. 
```


# Useful links

[AWS reference manual](https://docs.aws.amazon.com/fr_fr/cli/latest/reference/)